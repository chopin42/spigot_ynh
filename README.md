# Minecraft Spigot for YunoHost
This is a package for installing Minecraft Spigot on a server running YunoHost or Debian.

## Requirement
* A root SSH access to your server
* A server running a Debian-based OS

## Installation
### On YunoHost

```bash
sudo yunohost app install https://gitea.com/chopin42/spigot_ynh
sudo yunohost firewall allow TCP 25565
sudo yunohost firewall allow TCP 25575
```

## On Debian
Soon...

## Manage your server

* `systemctl start spigot` : Start the server
* `systemctl stop spigot` : Stop the server
* `systemctl restart spigot` : Restart the server
* `systemctl enable spigot` : Makes the server auto-start when booting (default)
* `systemctl disable spigot` : Do the opposite. 
* `systemctl status spigot` : Check the status of the app

## Configuration
Soon...

## Uninstallation
### On YunoHost

```bash
sudo yunohost app remove spigot
```

### On Debian
Soon...
